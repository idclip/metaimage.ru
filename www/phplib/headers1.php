<?php
//phpinfo();
/******************************************************************************/
/*    This file contains functions, that used to create
      standart view of your html-documents in current site.

      To prove, that your document looks like all other documents
      of this site, write the following lines into your document:

      <php? 
        require("headers.php");
        head("Write Your title string here");
      ?>
      - into the very beginning of your file;

      <?php foot(); ?>
      - into the very end of your file.                                       */

/*
$expert_list    = array(
                    "Irina_Korunkova"     => array(
                                                   "����� ���������",
                                                   "������������ �������, ������",
                                                   "������ ����� �� ���� ������ � ���� �� ������ ������� ������ � �������� ������"
                                                   ),
                    "Olga_Nelt"           => array(
                                                   "����� ����",
                                                   "��������, ��������-��������",
                                                   "������� �� �������� ����� �������������� � �������������� ������"
                                                   ),
                    "Anna_Lyanskaya"      => array(
                                                   "���� �������",
                                                   "��������, ���������, ������������, ��������",
                                                   "???�������������� � ���������???"
                                                   ),
                    "Inga_Admiralskaya"   => array(
                                                   "���� ������������",
                                                   "�������� ��������������� ����, �������������, ���������� � ������� �������",
                                                   "������ � �������"
                                                   ),
                    "Olga_Ershova"        => array(
                                                   "����� ������",
                                                   "��������, ����������",
                                                   "�������� � �������"
                                                   ),
                    "Irina_Maltseva"      => array(
                                                   "����� �������� (������)",
                                                   "��������, ���������� �� ������������-�������� �������, ������� ����� �� ���������� ��������",
                                                   "������������ ��������� ������ ����: �������, �����, ������"
                                                   ),
                    "IDEAclass"           => array(
                                                   "����� �������� ������� IDEA-class",
                                                   "������� ���������� - ���������� ����� �������� ������� IDEA-class, �����-�������",
                                                   "���� ������������� �����-��������"
                                                   ),
                    "Anastasia_Kamaeva"   => array(
                                                   "��������� �������",
                                                   "������������ ��������, ������������ ����� ��� ������� ����������, ������� ��� ���������������� ''���������'', ���������� ������� �� ����������������� ��������������� ''���� �������''",
                                                   "�������� � ��������������"
                                                   ),
                    "Kira_Feklisova"      => array(
                                                   "���� ���������",
                                                   "����-����, �����-����, ����������� �������, ������������� ������� ����� �������� ������� IDEA-class",
                                                   "�������� � ������"
                                                   ),
                    "Secret_Expert"       => array(
                                                   "������ �������",
                                                   "��������. �������������. �����������������. � �����������-���������������. �������������� �� �������������. ��������� �� ����������. �������� ������� ������������� �������. ����-������ � ������-������.",
                                                   "???�����������-��������������� ��������???"
                                                   ),
                    "Natalia_Korotovskih" => array(
                                                   "������� �����������",
                                                   "���������� �� �������-����������, ��������� ��������-��������, ������, ������-�������",
                                                   "�������� � ����� ������ ����������� �������"
                                                   ),
                    "Lubov_Molotkova"     => array(
                                                   "������ ���������",
                                                   "�������� ������ � �����-�������",
                                                   "������ ��������� � �������� �� ������������ �������"
                                                   ),
                    "Alina_Shmaneva"      => array(
                                                   "����� �������",
                                                   "������� ��������, ������� ����� � ������� ������� � ���������� � ��������� � ����� �������� ������� IDEA-class",
                                                   "������ ��������� � ������� ���������� � �������������"
                                                   ),
                    "Dina_Kareva"         => array(
                                                   "���� ������",
                                                   "�������� ��������",
                                                   "������ ���������������� �� ����������� �������� � ������� ������ � ������ �������"
                                                   ),
                    "Svetlana_Komarova"   => array(
                                                   "�������� ��������",
                                                   "������-�����������, ������� � ������� ������ ���������",
                                                   "�������� ������� ���������"
                                                   ),
                    "Elena_Hegai"         => array(
                                                   "����� �����",
                                                   "���������� ��������, ������-������, �����������, ���� IAF (������������� ���������� �������������), ����� � ������� ��������������� ����������������� ���",
                                                   "�������� � ����������������� ����"
                                                   ),
                    "Olga_Kryshchenko"    => array(
                                                   "����� ��������",
                                                   "���������� �������������, ��������-�����������",
                                                   "������� ������ ��������"
                                                   ),
                    "Natalia_Nosevich"    => array(
                                                   "������� �������",
                                                   "����������-��������",
                                                   "�������� � ����� ��������"
                                                   ),
                    "Olga_Redko"          => array(
                                                   "����� ������",
                                                   "���������, ������-����, ����, ������ ���� ������� � ��. ��. ��",
                                                   ""
                                                   ),
                    "Natalia_Zinina"      => array(
                                                   "������� ������",
                                                   "",
                                                   ""
                                                   ),
                    "Lamia"               => array(
                                                   "�����",
                                                   "�������� ���������",
                                                   "��������� � ���������� ��� ���������"
                                                   ),
                    "Amulet"              => array(
                                                   "������",
                                                   "��������� ��������� ��� ���������� ������� � ������ � ����� �����",
                                                   "����� ���������"
                                                   )
                    );
*/
$expert_list    = array(
                    "Irina_Korunkova"     => array(
                                                   "����� ���������",
                                                   "������������ �������, ������",
                                                   ""
                                                   ),
                    "Olga_Nelt"           => array(
                                                   "����� ����",
                                                   "��������, ��������-��������",
                                                   ""
                                                   ),
                    "Anna_Lyanskaya"      => array(
                                                   "���� �������",
                                                   "��������, ���������, ������������, ��������",
                                                   ""
                                                   ),
                    "Inga_Admiralskaya"   => array(
                                                   "���� ������������",
                                                   "�������� ��������������� ����, �������������, ���������� � ������� �������",
                                                   ""
                                                   ),
                    "Olga_Ershova"        => array(
                                                   "����� ������",
                                                   "��������, ����������",
                                                   ""
                                                   ),
                    "Irina_Maltseva"      => array(
                                                   "����� �������� (������)",
                                                   "��������, ���������� �� ������������-�������� �������, ������� ����� �� ���������� ��������",
                                                   ""
                                                   ),
                    "IDEAclass"           => array(
                                                   "����� �������� ������� IDEA-class",
                                                   "������� ���������� - ���������� ����� �������� ������� IDEA-class, �����-�������",
                                                   ""
                                                   ),
                    "Anastasia_Kamaeva"   => array(
                                                   "��������� �������",
                                                   "������������ ��������, ������������ ����� ��� ������� ����������, ������� ��� ���������������� ''���������'', ���������� ������� �� ����������������� ��������������� ''���� �������''",
                                                   ""
                                                   ),
                    "Kira_Feklisova"      => array(
                                                   "���� ���������",
                                                   "����-����, �����-����, ����������� �������, ������������� ������� ����� �������� ������� IDEA-class",
                                                   ""
                                                   ),
                    "Secret_Expert"       => array(
                                                   "������ �������",
                                                   "��������. �������������. �����������������. � �����������-���������������. �������������� �� �������������. ��������� �� ����������. �������� ������� ������������� �������. ����-������ � ������-������.",
                                                   ""
                                                   ),
                    "Natalia_Korotovskih" => array(
                                                   "������� �����������",
                                                   "���������� �� �������-����������, ��������� ��������-��������, ������, ������-�������",
                                                   ""
                                                   ),
                    "Lubov_Molotkova"     => array(
                                                   "������ ���������",
                                                   "�������� ������ � �����-�������",
                                                   ""
                                                   ),
                    "Alina_Shmaneva"      => array(
                                                   "����� �������",
                                                   "������� ��������, ������� ����� � ������� ������� � ���������� � ��������� � ����� �������� ������� IDEA-class",
                                                   ""
                                                   ),
                    "Dina_Kareva"         => array(
                                                   "���� ������",
                                                   "�������� ��������",
                                                   ""
                                                   ),
                    "Svetlana_Komarova"   => array(
                                                   "�������� ��������",
                                                   "������-�����������, ������� � ������� ������ ���������",
                                                   ""
                                                   ),
                    "Olga_Redko"          => array(
                                                   "����� ������",
                                                   "���������� ����� �������. ����������� ���������. ������ ���� Genesis.",
                                                   ""
                                                   ),
                    "Olga_Kryshchenko"    => array(
                                                   "����� ��������",
                                                   "���������� �������������, ��������-�����������",
                                                   ""
                                                   ),
                    "Natalia_Nosevich"    => array(
                                                   "������� �������",
                                                   "����������-��������",
                                                   ""
                                                   ),
                     /*
                    "Elena_Hegai"         => array(
                                                   "����� �����",
                                                   "���������� ��������, ������-������, �����������, ���� IAF (������������� ���������� �������������), ����� � ������� ��������������� ����������������� ���",
                                                   ""
                                                   ),
                    "Natalia_Zinina"      => array(
                                                   "������� ������",
                                                   "",
                                                   ""
                                                   ),
                     */
                     
                    "Lamia"               => array(
                                                   "�����",
                                                   "�������� ���������",
                                                   ""
                                                   ),
                    "Amulet"              => array(
                                                   "������",
                                                   "��������� ��������� ��� ���������� ������� � ������ � ����� �����",
                                                   ""
                                                   )
                    );
$archetype_list = array(
                   "01_Seeker"    => array(
                                           "��������",
                                           "������",
                                           "� ������ ��������� ������� ��������.",
                                           "������ �������� �������� �������� �� �������: �������������, ������������������, ��������. ������� �� ����������: Wanderer, Seeker, Explorer."
                                           ),
                   "02_Sage"      => array(
                                           "������",
                                           "�������",
                                           "������� - ������.",
                                           "������ �������� �������� �������� �� �������: �������, ��������. ������� �� ����������: Sage, Mage."
                                           ),
                   "03_Magician"  => array(
                                           "���",
                                           "����",
                                           "���� - ��������� ������� ����",
                                           "������ �������� �������� �������� �� �������: ���������, ����. ������� �� ����������: Magician."
                                           ),
                   "04_Caregiver" => array(
                                           "������",
                                           "������",
                                           "������ - ������� ������",
                                           "������ �������� �������� �������� �� �������: ���������, ���������. ������� �� ����������: Caregiver, Altruist."
                                           ),
                   "05_Ruler"     => array(
                                           "���������",
                                           "���",
                                           "��� - ���������",
                                           "������ �������� �������� �������� �� �������: -. ������� �� ����������: Ruler."
                                           ),
                   "06_Creator"   => array(
                                           "������",
                                           "����",
                                           "���� - ������",
                                           "������ �������� �������� �������� �� �������: ���������, �������. ������� �� ����������: Creator."
                                           ),
                   "07_Lover"     => array(
                                           "��������",
                                           "����",
                                           "���� - ��������",
                                           "������ �������� �������� �������� �� �������: �������, ��������, �����. ������� �� ����������: Lover."
                                           ),
                   "08_Orphan"    => array(
                                           "������� �����",
                                           "������",
                                           "������ - ������� �����",
                                           "������ �������� �������� �������� �� �������: ������, ���� ������. ������� �� ����������: Orphan, Regular Guy/Gal."
                                           ),
                   "09_Innocent"  => array(
                                           "������������",
                                           "��������",
                                           "�������� - ������������",
                                           "������ �������� �������� �������� �� �������: ��������. ������� �� ����������: Innocent."
                                           ),
                   "10_Hero"      => array(
                                           "����",
                                           "�������",
                                           "������� - ����",
                                           "������ �������� �������� �������� �� �������: �����. ������� �� ����������: Hero, Warrior."
                                           ),
                   "11_Outlaw"    => array(
                                           "�������",
                                           "������",
                                           "������ - �������",
                                           "������ �������� �������� �������� �� �������: �����������, �����. ������� �� ����������: Destroyer, Outlaw, Rebel."
                                           ),
                   "12_Jester"    => array(
                                           "���",
                                           "�������",
                                           "������� - ���",
                                           "������ �������� �������� �������� �� �������: ������, ��������, ����������. ������� �� ����������: Fool, Jester."
                                           )
                   );

/******************************************************************************/
/*                            PRIVATE BLOCK                                   */
/******************************************************************************/
/* ��������� HTML-��������� */
function html_head($title){
?>
<HEAD>
  <TITLE>[hloflo] <?php echo $title;?></TITLE>
  <LINK rel='shortcut icon' href='/phplib/inc/l.bmp'>
  <META http-equiv='Content-Type' content='text/html; charset=windows-1251'>
  <LINK href="/phplib/styles.css" rel="stylesheet" type="text/css" />
  <SCRIPT language="JavaScript" type="text/JavaScript">
  <!--
  function subDisplay(subName, sub1, sub2, sub3) {
    eval(subName).style.visibility = "visible"
    eval(subName).style.display = "block";
    eval(subName + 's').style.backgroundColor = "b4bcd4";

    eval(sub1).style.visibility = "hidden"
    eval(sub1).style.display    = "none";
    eval(sub1 + 's').style.backgroundColor = "92a8e5";
    eval(sub2).style.visibility = "hidden"
    eval(sub2).style.display    = "none";
    eval(sub2 + 's').style.backgroundColor = "92a8e5";
    eval(sub3).style.visibility = "hidden"
    eval(sub3).style.display    = "none";
    eval(sub3 + 's').style.backgroundColor = "92a8e5";
    
   }
  //-->
  </SCRIPT>
</HEAD>
<?php
}

/******************************************************************************/
function unauth_head( $title ){
/*   include('counter/count.php');*/
   counter();
?>
<HTML>
<?php  html_head($title); ?>

<BODY topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 marginheight=0 marginwidth=0 bgcolor=#ffffff>

<!-- TABLEHEADER -->
<TABLE width=100% border=0 cellpadding=0 cellspacing=0>
<TR>
<TD class=bgleft  height=100 >&nbsp;</TD>
<TD width=1024 height=100 class="head-text"><SPAN class=expert>�������� ������ ���<br>������� ����������������� ������</SPAN><BR>
<A href="/experts.htm">��������</A> <A href="/archetypes.htm">��������</A> <A href="/theory.htm">������</A> <A href="/library.htm">����������</A>
</TD>
<TD class=bgright height=100 >&nbsp;</TD>
</TR>
</TABLE>
<!-- TABLEHEADER END -->
<BR>

<!-- TABLEZEBRA -->
<TABLE width=100% border=0  cellpadding=0 cellspacing=0>
<!-- ������-� (����������+����) -->
<TR>
  <!-- ����� �������   - ���� � ���� -->
  <TD width=100 rowspan=2>&nbsp;</TD>
  <!-- ������� ������� - ����� ��� ������������ ���������� -->
  <TD width=* height=37 background='/phplib/inc/left-over203x37.gif' class=over>&nbsp;</TD>
  <!-- ������ �������  - ��� ����, ������ - �������-���� -->
  <TD width=100 rowspan=2>&nbsp;</TD>
</TR>
<TR><TD>
<!-- �������-���� -->
<TABLE width=1024 border=0  cellpadding=0 cellspacing=0 align=center>
<TR><TD>


<?php 
}

/******************************************************************************/
/*                            PUBLIC BLOCK                                    */
/******************************************************************************/
/* ������� */
function expert( $expert_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline src="/pictures/experts/<?php echo $expert_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><SPAN class=expert><?php echo $GLOBALS['expert_list'][$expert_code][0];?></SPAN></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['expert_list'][$expert_code][1];?></SPAN></TD></TR>
       <TR><TD class=center height=50><?php echo $GLOBALS['expert_list'][$expert_code][2];?></TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function expert_menu( $expert_code ){
?>
       <TABLE width=250>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline_e src="/pictures/experts/<?php echo $expert_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><SPAN class=expert_m><?php echo $GLOBALS['expert_list'][$expert_code][0];?></SPAN></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['expert_list'][$expert_code][1];?></SPAN></TD></TR>
       <TR><TD class=center>
           <TABLE width=100% border=0 cellpadding=0 cellspacing=0>
           <?
               foreach($GLOBALS['archetype_list'] as $key => $value) {
                  echo "<TR><TD class=micro><A href='/project/$key/$key" . "_" . $expert_code . ".htm'><IMG class=inline src='/pictures/icons/$key.png'>" . $value[1] . " - " . $value[0] . "</A></TD></TR>";
               }
           ?>
           </TABLE>
       </TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function expert_menu_more( $expert_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline1 src="/pictures/experts/<?php echo $expert_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><SPAN class=expert_min><?php echo $GLOBALS['expert_list'][$expert_code][0];?></SPAN></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['expert_list'][$expert_code][1];?></SPAN></TD></TR>
       <TR><TD class=center>
           ��� ������ ��������
           <TABLE width=100% border=0 cellpadding=0 cellspacing=0>
           <?
               foreach($GLOBALS['archetype_list'] as $key => $value) {
                  echo "<TR><TD class=micro><A href='/project/$key/$key" . "_" . $expert_code . ".htm'>" . $value[1] . " - " . $value[0] . "</A></TD></TR>";
               }
           ?>
           </TABLE>
       </TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function expert_menu_more1( $expert_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline1 src="/pictures/experts/<?php echo $expert_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><SPAN class=expert_min><?php echo $GLOBALS['expert_list'][$expert_code][0];?></SPAN></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['expert_list'][$expert_code][1];?></SPAN></TD></TR>
       </TABLE>
<?
}
/* �������-������� */
function expert_table( $expert_code ){
?>
       <TABLE align=center>
          <TR><TD class=center>
           <TABLE border=0 cellpadding=0 cellspacing=0>
           <?
               $i = 0;
               foreach($GLOBALS['archetype_list'] as $key => $value) {
                  $i++;
                  if ($i % 2 == 1){
                    echo "<TR>\n";
                  }
                  echo "<TD class=nojc>" .
                         "<A href='/project/$key/$key" . "_" . $expert_code . ".htm'><IMG class=inline src='/pictures/iconsbig/$key.png'></A><BR>" . 
                         "<A href='/project/$key/$key" . "_" . $expert_code . ".htm'><SPAN class=expert>" . $value[1] . " - " . $value[0] . "</SPAN></A>" . 
                       "</TD>\n";
                  if ($i % 2 == 0){
                    echo "</TR>\n";
                  }
               }
           ?>
          </TABLE>
          </TD></TR>
       </TABLE>       
<?
}
/* ������� */
function archetype( $archetype_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline src="/pictures/archetypes/<?php echo $archetype_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><SPAN class=expert><?php echo $GLOBALS['archetype_list'][$archetype_code][0];?></SPAN></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['archetype_list'][$archetype_code][2];?></SPAN></TD></TR>
       <TR><TD class=center height=50><?php echo $GLOBALS['archetype_list'][$archetype_code][3];?></TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function archetype_menu( $archetype_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline src="/pictures/archetypes/<?php echo $archetype_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><SPAN class=expert_m><?php echo $GLOBALS['archetype_list'][$archetype_code][0];?></SPAN></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['archetype_list'][$archetype_code][2];?></SPAN></TD></TR>
       <TR><TD class=center>
           <TABLE width=100% border=0 cellpadding=0 cellspacing=0>
           <?
               foreach($GLOBALS['expert_list'] as $key => $value) {
                  echo "<TR><TD class=micro><A href='/project/$archetype_code/$archetype_code" . "_" . $key . ".htm'><IMG class=inline src='/pictures/icons/$archetype_code.png'>" . $value[0] . "</A></TD></TR>";
               }
           ?>
           </TABLE>
       </TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function archetype_menu_more( $archetype_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline1 src="/pictures/archetypes/<?php echo $archetype_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><SPAN class=expert_min><?php echo $GLOBALS['archetype_list'][$archetype_code][0];?></SPAN></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['archetype_list'][$archetype_code][2];?></SPAN></TD></TR>
       <TR><TD class=center>
           ��� �� ��������:
           <TABLE width=100% border=0 cellpadding=0 cellspacing=0>
           <?
               foreach($GLOBALS['expert_list'] as $key => $value) {
                  echo "<TR><TD class=micro><A href='/project/$archetype_code/$archetype_code" . "_" . $key . ".htm'>" . $value[0] . "</A></TD></TR>";
               }
           ?>
           </TABLE>
       </TD></TR>
       </TABLE>
<?
}
/* �������-������� */
function archetype_table( $archetype_code ){
?>
       <TABLE align=center>
          <TR><TD class=center>
           <TABLE border=0 cellpadding=0 cellspacing=0>
           <?
               $i = 0;
               foreach($GLOBALS['expert_list'] as $key => $value) {
                  $i++;
                  if ($i % 2 == 1){
                    echo "<TR>\n";
                  }
                  echo "<TD class=nojc>" .
                         "<A href='/project/$archetype_code/$archetype_code" . "_" . $key . ".htm'><IMG class=inline_e src='/pictures/experts/$key.png'></A><BR>" . 
                         "<A href='/project/$archetype_code/$archetype_code" . "_" . $key . ".htm'><SPAN class=expert>" . $value[0] . "</SPAN></A>" . 
                       "</TD>\n";
                  if ($i % 2 == 0){
                    echo "</TR>\n";
                  }
               }
           ?>
          </TABLE>
          </TD></TR>
       </TABLE>       
<?
}

/******************************************************************************/
/* ��������� ���� ������� (��� ���� �����������) */
function section( $section_name ){
  global $vars;
  $vars->$section = $section_name;
}

/******************************************************************************/
/* �������� ����� ��������� */
function head( $title ){
  unauth_head($title);
}

/******************************************************************************/
/* �������� ������� ��������� */
function foot(){
?>
<BR></BR>
<CENTER><FONT size=-1>&copy; 2015-<? echo date('Y') ?> hloflo</FONT></CENTER>
<BR>
</TD></TR>
</TABLE>
<!-- ����� �������-���� -->
</TD></TR>
</TABLE>
<!-- TABLEZEBRA END -->

</BODY>
</HTML>
<?php
}

/******************************************************************************/
/* ������ �������� ����� */
function wide_block_start(){
?>
<BR>
</TD></TR>
</TABLE>
<!-- ����� �������-���� -->
</TD></TR>
</TABLE>
<!-- TABLEZEBRA END -->
<?php
}

/******************************************************************************/
/* ����� �������� ����� */
function wide_block_end(){
?>
<!-- TABLEZEBRA -->
<TABLE width=100% border=0  cellpadding=0 cellspacing=0>
<TR><TD>
<!-- �������-���� -->
<TABLE width=600 border=0  cellpadding=0 cellspacing=0 align=center>
<TR><TD>

<?php
}

/******************************************************************************/
function menu(){
 global $DOCUMENT_ROOT;
 $menu = new Menu();
 /* ���������� ���� ���� ����������, ���� ������ */
 $root_path = $_ENV["PHP_DOCUMENT_ROOT"] . $_SERVER["DOCUMENT_ROOT"];
 $menu->load_conf($root_path.'/phplib/menu.conf');
 $menu->print_menu();
}

/******************************************************************************/
function counter(){
//  $counter = new Counter();
//  $counter->add_this_page();
}
/******************************************************************************/


?>

