<?php
//phpinfo();
/******************************************************************************/
/*    This file contains functions, that used to create
      standart view of your html-documents in current site.

      To prove, that your document looks like all other documents
      of this site, write the following lines into your document:

      <php? 
        require("headers.php");
        head("Write Your title string here");
      ?>
      - into the very beginning of your file;

      <?php foot(); ?>
      - into the very end of your file.                                       */

$expert_list    = array(
                    "Irina_Korunkova"     => array(
                                                   "����� ���������",
                                                   "������������ �������, ������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>1, "10_Hero"=>1, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Olga_Nelt"           => array(
                                                   "����� ����",
                                                   "��������, ��������-��������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>1, "10_Hero"=>1, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Anna_Lyanskaya"      => array(
                                                   "���� �������",
                                                   "��������, ���������, ������������, ��������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Inga_Admiralskaya"   => array(
                                                   "���� ������������",
                                                   "�������� ��������������� ����, �������������, ���������� � ������� �������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>1, "10_Hero"=>1, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Olga_Ershova"        => array(
                                                   "����� ������",
                                                   "��������, ����������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>0, "08_Orphan"=>1, 
                                                         "09_Innocent"=>1, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Irina_Maltseva"      => array(
                                                   "����� �������� (������)",
                                                   "��������, ���������� �� ������������-�������� �������, ������� ����� �� ���������� ��������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "IDEAclass"           => array(
                                                   "����� �������� ������� IDEA-class",
                                                   "������� ���������� - ���������� ����� �������� ������� IDEA-class, �����-�������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Anastasia_Kamaeva"   => array(
                                                   "��������� �������",
                                                   "������������ ��������, ������������ ����� ��� ������� ����������, ������� ��� ���������������� ''���������'', ���������� ������� �� ����������������� ��������������� ''���� �������''",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>1, "10_Hero"=>1, "11_Outlaw"=>1, "12_Jester"=>0)
                                                   ),
                    "Kira_Feklisova"      => array(
                                                   "���� ���������",
                                                   "����-����, �����-����, ����������� �������, ������������� ������� ����� �������� ������� IDEA-class",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>0, 
                                                         "09_Innocent"=>1, "10_Hero"=>1, "11_Outlaw"=>1, "12_Jester"=>0)
                                                   ),
                    "Secret_Expert"       => array(
                                                   "������ �������",
                                                   "��������. �������������. �����������������. � �����������-���������������. �������������� �� �������������. ��������� �� ����������. �������� ������� ������������� �������. ����-������ � ������-������.",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>0, "07_Lover"=>0, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Natalia_Korotovskih" => array(
                                                   "������� �����������",
                                                   "���������� �� �������-����������, ��������� ��������-��������, ������, ������-�������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Lubov_Molotkova"     => array(
                                                   "������ ���������",
                                                   "�������� ������ � �����-�������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Alina_Shmaneva"      => array(
                                                   "����� �������",
                                                   "������� ��������, ������� ����� � ������� ������� � ���������� � ��������� � ����� �������� ������� IDEA-class",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>0, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>1, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Dina_Kareva"         => array(
                                                   "���� ������",
                                                   "�������� ��������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>0, "06_Creator"=>0, "07_Lover"=>0, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Svetlana_Komarova"   => array(
                                                   "�������� ��������",
                                                   "������-�����������, ������� � ������� ������ ���������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Olga_Redko"          => array(
                                                   "����� ������",
                                                   "���������� ����� �������. ����������� ���������. ������ ���� Genesis.",
                                                   "",
                                                   array("01_Seeker"=>0, "02_Sage"=>0, "03_Magician"=>0, "04_Caregiver"=>0, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>1, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Olga_Kryshchenko"    => array(
                                                   "����� ��������",
                                                   "���������� �������������, ��������-�����������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>0, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Natalia_Nosevich"    => array(
                                                   "������� �������",
                                                   "����������-��������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>0, "06_Creator"=>1, "07_Lover"=>0, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Lamia"               => array(
                                                   "�����",
                                                   "�������� ���������",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>1, "08_Orphan"=>1, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   ),
                    "Amulet"              => array(
                                                   "������",
                                                   "��������� ��������� ��� ���������� ������� � ������ � ����� �����",
                                                   "",
                                                   array("01_Seeker"=>1, "02_Sage"=>1, "03_Magician"=>1, "04_Caregiver"=>1, 
                                                         "05_Ruler"=>1, "06_Creator"=>1, "07_Lover"=>0, "08_Orphan"=>0, 
                                                         "09_Innocent"=>0, "10_Hero"=>0, "11_Outlaw"=>0, "12_Jester"=>0)
                                                   )
                    );
$archetype_list = array(
                   "01_Seeker"    => array(
                                           "��������",
                                           "������",
                                           "� ������ ��������� ������� ��������.",
                                           "������ �������� �������� �������� �� �������: �������������, ������������������, ��������. ������� �� ����������: Wanderer, Seeker, Explorer."
                                           ),
                   "02_Sage"      => array(
                                           "������",
                                           "�������",
                                           "������� - ������.",
                                           "������ �������� �������� �������� �� �������: �������, ��������. ������� �� ����������: Sage, Mage."
                                           ),
                   "03_Magician"  => array(
                                           "���",
                                           "����",
                                           "���� - ��������� ������� ����",
                                           "������ �������� �������� �������� �� �������: ���������, ����. ������� �� ����������: Magician."
                                           ),
                   "04_Caregiver" => array(
                                           "������",
                                           "������",
                                           "������ - ������� ������",
                                           "������ �������� �������� �������� �� �������: ���������, ���������. ������� �� ����������: Caregiver, Altruist."
                                           ),
                   "05_Ruler"     => array(
                                           "���������",
                                           "���",
                                           "��� - ���������",
                                           "������ �������� �������� �������� �� �������: -. ������� �� ����������: Ruler."
                                           ),
                   "06_Creator"   => array(
                                           "������",
                                           "����",
                                           "���� - ������",
                                           "������ �������� �������� �������� �� �������: ���������, �������. ������� �� ����������: Creator."
                                           ),
                   "07_Lover"     => array(
                                           "��������",
                                           "����",
                                           "���� - ��������",
                                           "������ �������� �������� �������� �� �������: �������, ��������, �����. ������� �� ����������: Lover."
                                           ),
                   "08_Orphan"    => array(
                                           "������� �����",
                                           "������",
                                           "������ - ������� �����",
                                           "������ �������� �������� �������� �� �������: ������, ���� ������. ������� �� ����������: Orphan, Regular Guy/Gal."
                                           ),
                   "09_Innocent"  => array(
                                           "������������",
                                           "��������",
                                           "�������� - ������������",
                                           "������ �������� �������� �������� �� �������: ��������. ������� �� ����������: Innocent."
                                           ),
                   "10_Hero"      => array(
                                           "����",
                                           "�������",
                                           "������� - ����",
                                           "������ �������� �������� �������� �� �������: �����. ������� �� ����������: Hero, Warrior."
                                           ),
                   "11_Outlaw"    => array(
                                           "�������",
                                           "������",
                                           "������ - �������",
                                           "������ �������� �������� �������� �� �������: �����������, �����. ������� �� ����������: Destroyer, Outlaw, Rebel."
                                           ),
                   "12_Jester"    => array(
                                           "���",
                                           "�������",
                                           "������� - ���",
                                           "������ �������� �������� �������� �� �������: ������, ��������, ����������. ������� �� ����������: Fool, Jester."
                                           )
                   );

/******************************************************************************/
/*                            PRIVATE BLOCK                                   */
/******************************************************************************/
/* ��������� HTML-��������� */
function html_head($title){
?>
<HEAD>
  <TITLE>��������: ����������������� ������-�����������</TITLE>
  <LINK rel='shortcut icon' href='/phplib/inc/l.bmp'>
  <META http-equiv='Content-Type' content='text/html; charset=windows-1251'>
  <LINK href="/phplib/styles.css" rel="stylesheet" type="text/css" />
  <SCRIPT language="JavaScript" type="text/JavaScript">
  <!--
  function subDisplay(subName, sub1, sub2, sub3) {
    eval(subName).style.visibility = "visible"
    eval(subName).style.display = "block";
    eval(subName + 's').style.backgroundColor = "b4bcd4";

    eval(sub1).style.visibility = "hidden"
    eval(sub1).style.display    = "none";
    eval(sub1 + 's').style.backgroundColor = "92a8e5";
    eval(sub2).style.visibility = "hidden"
    eval(sub2).style.display    = "none";
    eval(sub2 + 's').style.backgroundColor = "92a8e5";
    eval(sub3).style.visibility = "hidden"
    eval(sub3).style.display    = "none";
    eval(sub3 + 's').style.backgroundColor = "92a8e5";
    
   }
  //-->
  </SCRIPT>
</HEAD>
<?php
}

/******************************************************************************/
function unauth_head( $title ){
/*   include('counter/count.php');*/
   counter();
?>
<HTML>
<?php  html_head($title); ?>

<BODY topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 marginheight=0 marginwidth=0 bgcolor=#ffffff>

<!-- TABLEHEADER -->
<TABLE width=100% border=0 cellpadding=0 cellspacing=0>
<TR>
<TD class=bgleft  height=100 ><img class=inline height=300 src='/pictures/menu/ArchLeft.png'></TD>
<TD width=1024 height=100 class="head-text"><a href="/"><IMG class=inline src="/pictures/menu/Metaimage600w.png"></a><BR>
<A href="/experts.htm"><img class=inline src="/pictures/menu/Experts20.png"></A> <A href="/archetypes.htm"><img class=inline src="/pictures/menu/Archetypes20.png"></A> <A href="/theory.htm"><img class=inline src="/pictures/menu/Theory20.png"></A> <A href="/library.htm"><img class=inline src="/pictures/menu/Library20.png"></A>
</TD>
<TD class=bgright height=100 ><img class=inline height=300 src='/pictures/menu/ArchRight.png'></TD>
</TR>
</TABLE>
<!-- TABLEHEADER END -->
<BR>

<!-- TABLEZEBRA -->
<TABLE width=100% border=0  cellpadding=0 cellspacing=0>
<!-- ������-� (����������+����) -->
<TR>
  <!-- ����� �������   - ���� � ���� -->
  <TD width=100 rowspan=2>&nbsp;</TD>
  <!-- ������� ������� - ����� ��� ������������ ���������� -->
  <TD width=* height=37 background='/phplib/inc/left-over203x37.gif' class=over>&nbsp;</TD>
  <!-- ������ �������  - ��� ����, ������ - �������-���� -->
  <TD width=100 rowspan=2>&nbsp;</TD>
</TR>
<TR><TD>
<!-- �������-���� -->
<TABLE width=1024 border=0  cellpadding=0 cellspacing=0 align=center>
<TR><TD>


<?php 
}

/******************************************************************************/
/*                            PUBLIC BLOCK                                    */
/******************************************************************************/
/* ������� */
function expert( $expert_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline src="/pictures/experts/<?php echo $expert_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline src="/pictures/experts/<?php echo $expert_code;?>20.png"></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['expert_list'][$expert_code][1];?></SPAN></TD></TR>
       <TR><TD class=center height=50><?php echo $GLOBALS['expert_list'][$expert_code][2];?></TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function expert_menu( $expert_code ){
?>
       <TABLE width=250>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline_e src="/pictures/experts/<?php echo $expert_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline src="/pictures/experts/<?php echo $expert_code;?>12.png"></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['expert_list'][$expert_code][1];?></SPAN></TD></TR>
       <TR><TD class=center>
           <TABLE width=100% border=0 cellpadding=0 cellspacing=0>
           <?
               foreach($GLOBALS['archetype_list'] as $key => $value) {
                  if($GLOBALS['expert_list'][$expert_code][3][$key] == 1){
                     echo "<TR><TD class=micro><A href='/project/$key/$key" . "_" . $expert_code . ".htm'><IMG class=inline src='/pictures/icons/$key.png'>" . $value[1] . " - " . $value[0] . "</A></TD></TR>";
                  }
               }
           ?>
           </TABLE>
       </TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function expert_menu_more( $expert_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline1 src="/pictures/experts/<?php echo $expert_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline src="/pictures/experts/<?php echo $expert_code;?>12.png"></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['expert_list'][$expert_code][1];?></SPAN></TD></TR>
       <TR><TD class=center>
           ��� ������ ��������
           <TABLE width=100% border=0 cellpadding=0 cellspacing=0>
           <?
               foreach($GLOBALS['archetype_list'] as $key => $value) {
                  if($GLOBALS['expert_list'][$expert_code][3][$key] == 1){
                     echo "<TR><TD class=micro><A href='/project/$key/$key" . "_" . $expert_code . ".htm'>" . $value[1] . " - " . $value[0] . "</A></TD></TR>";
                  }
               }
           ?>
           </TABLE>
       </TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function expert_menu_more1( $expert_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><IMG class=inline1 src="/pictures/experts/<?php echo $expert_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/experts/<?php echo $expert_code;?>.htm"><SPAN class=expert_min><?php echo $GLOBALS['expert_list'][$expert_code][0];?></SPAN></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['expert_list'][$expert_code][1];?></SPAN></TD></TR>
       </TABLE>
<?
}
/* �������-������� */
function expert_table( $expert_code ){
?>
       <TABLE align=center>
          <TR><TD class=center>
           <TABLE border=0 cellpadding=0 cellspacing=0>
           <?
               $i = 0;
               foreach($GLOBALS['archetype_list'] as $key => $value) {
                  $i++;
                  if ($i % 2 == 1){
                    echo "<TR>\n";
                  }
                  echo "<TD class=nojc>";
                  if($GLOBALS['expert_list'][$expert_code][3][$key] == 1){
                  echo    "<A href='/project/$key/$key" . "_" . $expert_code . ".htm'><IMG class=inline src='/pictures/iconsbig/$key.png'></A><BR>";
                  echo    "<A href='/project/$key/$key" . "_" . $expert_code . ".htm'><SPAN class=expert><IMG class=inline src='/pictures/archetypes/" . $key . "20.png'></A>";
                  }
                  echo "</TD>\n";
                  if ($i % 2 == 0){
                    echo "</TR>\n";
                  }
               }
           ?>
          </TABLE>
          </TD></TR>
       </TABLE>       
<?
}
/* ������� */
function archetype( $archetype_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline src="/pictures/archetypes/<?php echo $archetype_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline src="/pictures/archetypes/<?php echo $archetype_code;?>20.png"></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['archetype_list'][$archetype_code][2];?></SPAN></TD></TR>
       <TR><TD class=center height=50><?php echo $GLOBALS['archetype_list'][$archetype_code][3];?></TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function archetype_menu( $archetype_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline src="/pictures/archetypes/<?php echo $archetype_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline src="/pictures/archetypes/<?php echo $archetype_code;?>20.png"></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['archetype_list'][$archetype_code][2];?></SPAN></TD></TR>
       <TR><TD class=center>
           <TABLE width=100% border=0 cellpadding=0 cellspacing=0>
           <?
               foreach($GLOBALS['expert_list'] as $key => $value) {
                  if($GLOBALS['expert_list'][$key][3][$archetype_code] == 1){
                     echo "<TR><TD class=micro><A href='/project/$archetype_code/$archetype_code" . "_" . $key . ".htm'><IMG class=inline src='/pictures/icons/$archetype_code.png'>" . $value[0] . "</A></TD></TR>";
                  }
               }
           ?>
           </TABLE>
       </TD></TR>
       </TABLE>
<?
}
/* �������-���� */
function archetype_menu_more( $archetype_code ){
?>
       <TABLE width=100%>
       <TR><TD class=center>&nbsp;</TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline1 src="/pictures/archetypes/<?php echo $archetype_code;?>.png"></A></TD></TR>
       <TR><TD class=center><A href="/archetypes/<?php echo $archetype_code;?>.htm"><IMG class=inline src="/pictures/archetypes/<?php echo $archetype_code;?>12.png"></A></TD></TR>
       <TR><TD class=center><SPAN class=expertdesc><?php echo $GLOBALS['archetype_list'][$archetype_code][2];?></SPAN></TD></TR>
       <TR><TD class=center>
           ��� �� ��������:
           <TABLE width=100% border=0 cellpadding=0 cellspacing=0>
           <?
               foreach($GLOBALS['expert_list'] as $key => $value) {
                  if($GLOBALS['expert_list'][$key][3][$archetype_code] == 1){
                     echo "<TR><TD class=micro><A href='/project/$archetype_code/$archetype_code" . "_" . $key . ".htm'>" . $value[0] . "</A></TD></TR>";
                  }
               }
           ?>
           </TABLE>
       </TD></TR>
       </TABLE>
<?
}
/* �������-������� */
function archetype_table( $archetype_code ){
?>
       <TABLE align=center>
          <TR><TD class=center>
           <TABLE border=0 cellpadding=0 cellspacing=0>
           <?
               $i = 0;
               foreach($GLOBALS['expert_list'] as $key => $value) {
                  $i++;
                  if ($i % 2 == 1){
                    echo "<TR>\n";
                  }
                  echo "<TD class=nojc>" .
                         "<A href='/project/$archetype_code/$archetype_code" . "_" . $key . ".htm'><IMG class=inline_e src='/pictures/experts/$key.png'></A><BR>" . 
                         "<A href='/project/$archetype_code/$archetype_code" . "_" . $key . ".htm'><IMG class=inline   src='/pictures/experts/" . $key . "20.png'></A>" . 
                       "</TD>\n";
                  if ($i % 2 == 0){
                    echo "</TR>\n";
                  }
               }
           ?>
          </TABLE>
          </TD></TR>
       </TABLE>       
<?
}

/******************************************************************************/
/* ��������� ������ */
function project_header( $archetype_code, $expert_code, $expert_code2 = NULL ){

echo "<CENTER><IMG src='/pictures/experts/" . $expert_code . "20.png' class=inline>";
if ( $expert_code2 ) {
   echo "<IMG src='/pictures/experts/" . $expert_code2 . "20.png' class=inline>";
}
echo "</CENTER>";
echo "<CENTER><IMG class=inline src='/pictures/menu/Archetype.png'>&nbsp;<IMG class=inline src='/pictures/archetypes/" . $archetype_code . "20.png'></CENTER>";

}
/******************************************************************************/
/* ��������� ���� ������� (��� ���� �����������) */
function section( $section_name ){
  global $vars;
  $vars->$section = $section_name;
}

/******************************************************************************/
/* �������� ����� ��������� */
function head( $title ){
  unauth_head($title);
}

/******************************************************************************/
/* �������� ������� ��������� */
function foot(){
?>
<BR></BR>
<CENTER><FONT size=-1>&copy; 2015-<? echo date('Y') ?> hloflo</FONT></CENTER>
<BR>
</TD></TR>
</TABLE>
<!-- ����� �������-���� -->
</TD></TR>
</TABLE>
<!-- TABLEZEBRA END -->

</BODY>
</HTML>
<?php
}

/******************************************************************************/
/* ������ �������� ����� */
function wide_block_start(){
?>
<BR>
</TD></TR>
</TABLE>
<!-- ����� �������-���� -->
</TD></TR>
</TABLE>
<!-- TABLEZEBRA END -->
<?php
}

/******************************************************************************/
/* ����� �������� ����� */
function wide_block_end(){
?>
<!-- TABLEZEBRA -->
<TABLE width=100% border=0  cellpadding=0 cellspacing=0>
<TR><TD>
<!-- �������-���� -->
<TABLE width=600 border=0  cellpadding=0 cellspacing=0 align=center>
<TR><TD>

<?php
}

/******************************************************************************/
function menu(){
 global $DOCUMENT_ROOT;
 $menu = new Menu();
 /* ���������� ���� ���� ����������, ���� ������ */
 $root_path = $_ENV["PHP_DOCUMENT_ROOT"] . $_SERVER["DOCUMENT_ROOT"];
 $menu->load_conf($root_path.'/phplib/menu.conf');
 $menu->print_menu();
}

/******************************************************************************/
function counter(){
//  $counter = new Counter();
//  $counter->add_this_page();
}
/******************************************************************************/


?>

